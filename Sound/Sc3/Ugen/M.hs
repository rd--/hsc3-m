{-# LANGUAGE FlexibleInstances,TypeSynonymInstances #-}

module Sound.Sc3.Ugen.M where

import Data.Functor.Identity {- base -}
import Data.Maybe {- base -}

import Sound.Sc3.Common.Enum {- hsc3 -}
import Sound.Sc3.Common.Math {- hsc3 -}
import Sound.Sc3.Common.Rate {- hsc3 -}
import Sound.Sc3.Common.Uid {- hsc3 -}

import qualified Sound.Sc3 as Sc3 {- hsc3 -}

import qualified Sound.Sc3.Ugen.Dot as Dot {- hsc3-dot -}

-- * TYPE

newtype Ugen_M m = Ugen_M {mk_ugen :: m Sc3.Ugen}

type F1 t = t -> t
type F2 t = t -> t -> t
type F3 t = t -> t -> t -> t
type F4 t = t -> t -> t -> t -> t
type F5 t = t -> t -> t -> t -> t -> t
type F6 t = t -> t -> t -> t -> t -> t -> t
type F7 t = t -> t -> t -> t -> t -> t -> t -> t
type F8 t = t -> t -> t -> t -> t -> t -> t -> t -> t
type F9 t = t -> t -> t -> t -> t -> t -> t -> t -> t -> t
type F10 t = t -> t -> t -> t -> t -> t -> t -> t -> t -> t -> t

-- * LIFT

lift_ugen_0 :: Monad m => Sc3.Ugen -> Ugen_M m
lift_ugen_0 f = Ugen_M (return f)

lift_ugen_1 :: Monad m => F1 Sc3.Ugen -> F1 (Ugen_M m)
lift_ugen_1 f (Ugen_M p) = Ugen_M $ do
  p' <- p
  return (f p')

lift_ugen_2 :: Monad m => F2 Sc3.Ugen -> F2 (Ugen_M m)
lift_ugen_2 f (Ugen_M p) (Ugen_M q) = Ugen_M $ do
  p' <- p
  q' <- q
  return (f p' q')

lift_ugen_3 :: Monad m => F3 Sc3.Ugen -> F3 (Ugen_M m)
lift_ugen_3 f (Ugen_M p) (Ugen_M q) (Ugen_M r) = Ugen_M $ do
  p' <- p
  q' <- q
  r' <- r
  return (f p' q' r')

lift_ugen_4 :: Monad m => F4 Sc3.Ugen -> F4 (Ugen_M m)
lift_ugen_4 f (Ugen_M p) (Ugen_M q) (Ugen_M r) (Ugen_M s) = Ugen_M $ do
  p' <- p
  q' <- q
  r' <- r
  s' <- s
  return (f p' q' r' s')

lift_ugen_5 :: Monad m => F5 Sc3.Ugen -> F5 (Ugen_M m)
lift_ugen_5 f (Ugen_M p) (Ugen_M q) (Ugen_M r) (Ugen_M s) (Ugen_M t) = Ugen_M $ do
  p' <- p
  q' <- q
  r' <- r
  s' <- s
  t' <- t
  return (f p' q' r' s' t')

lift_ugen_6 :: Monad m => F6 Sc3.Ugen -> F6 (Ugen_M m)
lift_ugen_6 f (Ugen_M p) (Ugen_M q) (Ugen_M r) (Ugen_M s) (Ugen_M t) (Ugen_M u) = Ugen_M $ do
  p' <- p
  q' <- q
  r' <- r
  s' <- s
  t' <- t
  u' <- u
  return (f p' q' r' s' t' u')

lift_ugen_7 :: Monad m => F7 Sc3.Ugen -> F7 (Ugen_M m)
lift_ugen_7 f (Ugen_M p) (Ugen_M q) (Ugen_M r) (Ugen_M s) (Ugen_M t) (Ugen_M u) (Ugen_M v) = Ugen_M $ do
  p' <- p
  q' <- q
  r' <- r
  s' <- s
  t' <- t
  u' <- u
  v' <- v
  return (f p' q' r' s' t' u' v')

lift_ugen_8 :: Monad m => F8 Sc3.Ugen -> F8 (Ugen_M m)
lift_ugen_8 f (Ugen_M p) (Ugen_M q) (Ugen_M r) (Ugen_M s) (Ugen_M t) (Ugen_M u) (Ugen_M v) (Ugen_M w) = Ugen_M $ do
  p' <- p
  q' <- q
  r' <- r
  s' <- s
  t' <- t
  u' <- u
  v' <- v
  w' <- w
  return (f p' q' r' s' t' u' v' w')

lift_ugen_9 :: Monad m => F9 Sc3.Ugen -> F9 (Ugen_M m)
lift_ugen_9 f (Ugen_M p) (Ugen_M q) (Ugen_M r) (Ugen_M s) (Ugen_M t) (Ugen_M u) (Ugen_M v) (Ugen_M w) (Ugen_M x) = Ugen_M $ do
  p' <- p
  q' <- q
  r' <- r
  s' <- s
  t' <- t
  u' <- u
  v' <- v
  w' <- w
  x' <- x
  return (f p' q' r' s' t' u' v' w' x')


lift_ugen_10 :: Monad m => F10 Sc3.Ugen -> F10 (Ugen_M m)
lift_ugen_10 f (Ugen_M p) (Ugen_M q) (Ugen_M r) (Ugen_M s) (Ugen_M t) (Ugen_M u) (Ugen_M v) (Ugen_M w) (Ugen_M x) (Ugen_M y) = Ugen_M $ do
  p' <- p
  q' <- q
  r' <- r
  s' <- s
  t' <- t
  u' <- u
  v' <- v
  w' <- w
  x' <- x
  y' <- y
  return (f p' q' r' s' t' u' v' w' x' y')

-- * LIFT M

lift_ugenM_0 :: Monad m => m Sc3.Ugen -> Ugen_M m
lift_ugenM_0 = Ugen_M

lift_ugenM_1 :: Monad m => (Sc3.Ugen -> m Sc3.Ugen) -> Ugen_M m -> Ugen_M m
lift_ugenM_1 f (Ugen_M p) = Ugen_M $ do
  p' <- p
  f p'

lift_ugenM_2 :: Monad m => (Sc3.Ugen -> Sc3.Ugen -> m Sc3.Ugen) -> Ugen_M m -> Ugen_M m -> Ugen_M m
lift_ugenM_2 f (Ugen_M p) (Ugen_M q) = Ugen_M $ do
  p' <- p
  q' <- q
  f p' q'

lift_ugenM_3 :: Monad m => (Sc3.Ugen -> Sc3.Ugen -> Sc3.Ugen -> m Sc3.Ugen) -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
lift_ugenM_3 f (Ugen_M p) (Ugen_M q) (Ugen_M r) = Ugen_M $ do
  p' <- p
  q' <- q
  r' <- r
  f p' q' r'

lift_ugenM_4 :: Monad m => (Sc3.Ugen -> Sc3.Ugen -> Sc3.Ugen -> Sc3.Ugen -> m Sc3.Ugen) -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
lift_ugenM_4 f (Ugen_M p) (Ugen_M q) (Ugen_M r) (Ugen_M s) = Ugen_M $ do
  p' <- p
  q' <- q
  r' <- r
  s' <- s
  f p' q' r' s'

-- * INSTANCES

instance Monad m => Num (Ugen_M m) where
  (+) = lift_ugen_2 (+)
  (*) = lift_ugen_2 (*)
  (-) = lift_ugen_2 (-)
  abs = lift_ugen_1 abs
  signum = lift_ugen_1 signum
  negate = lift_ugen_1 negate
  fromInteger = Ugen_M . return . fromInteger

instance Monad m => Fractional (Ugen_M m) where
  (/) = lift_ugen_2 (/)
  recip = lift_ugen_1 recip
  fromRational = Ugen_M . return . fromRational

instance Monad m => Floating (Ugen_M m) where
  pi = lift_ugen_0 pi
  exp = lift_ugen_1 exp
  log = lift_ugen_1 log
  sqrt = lift_ugen_1 sqrt
  (**) = lift_ugen_2 (**)
  logBase = lift_ugen_2 logBase
  sin = lift_ugen_1 sin
  cos = lift_ugen_1 cos
  tan = lift_ugen_1 tan
  asin = lift_ugen_1 asin
  acos = lift_ugen_1 acos
  atan = lift_ugen_1 atan
  sinh = lift_ugen_1 sinh
  cosh = lift_ugen_1 cosh
  tanh = lift_ugen_1 tanh
  asinh = lift_ugen_1 asinh
  acosh = lift_ugen_1 acosh
  atanh = lift_ugen_1 atanh

instance Monad m => Eq (Ugen_M m) where
  (==) = error "Ugen: =="

instance Monad m => Ord (Ugen_M m) where
{-
    (<) = lift_ugen_2 (<)
    (<=) = lift_ugen_2 (<=)
    (>) = lift_ugen_2 (>)
    (>=) = lift_ugen_2 (>=)
-}
  compare = error "Ugen: compare"
  min = lift_ugen_2 min
  max = lift_ugen_2 max

instance Monad m => Sc3.UnaryOp (Ugen_M m) where
  cubed = lift_ugen_1 Sc3.cubed
  distort = lift_ugen_1 Sc3.distort
  midiCps = lift_ugen_1 Sc3.midiCps

class Uid m => Share m where
  share :: Ugen_M m -> Ugen_M m

instance Share Uid_St where
  share = Ugen_M . return . uid_st_eval . mk_ugen

type Ugen_Id = Ugen_M Identity
type Ugen = Ugen_M Uid_St

instance Sc3.Audible Ugen_Id where
    playAt opt (Ugen_M u) = Sc3.playUgen opt (uid_id_eval u)

instance Sc3.Audible Ugen where
    playAt opt (Ugen_M u) = Sc3.playUgen opt (uid_st_eval u)

instance Dot.Drawable Ugen_Id where
    dot_with_opt opt (Ugen_M u) = Dot.dot_with_opt opt (uid_id_eval u)

instance Dot.Drawable Ugen where
    dot_with_opt opt (Ugen_M u) = Dot.dot_with_opt opt (uid_st_eval u)

draw_id :: Ugen_Id -> IO ()
draw_id = Dot.draw

draw_st :: Ugen -> IO ()
draw_st = Dot.draw

audition_id :: Ugen_Id -> IO ()
audition_id = Sc3.audition

audition_st :: Ugen -> IO ()
audition_st = Sc3.audition

synthstat_wr :: Ugen -> IO ()
synthstat_wr = Sc3.synthstat_wr . uid_st_eval . mk_ugen

synthdef :: String -> Ugen -> Sc3.Synthdef
synthdef nm (Ugen_M u) = Sc3.synthdef nm (uid_st_eval u)

-- * UTIL

dinf :: Monad m => Ugen_M m
dinf = 9e8

dup :: Monad m => Int -> Ugen_M m -> Ugen_M m
dup n = mce . replicate n

mce :: Monad m => [Ugen_M m] -> Ugen_M m
mce x = Ugen_M (sequence (map mk_ugen x) >>= return . Sc3.mce)

mce2 :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
mce2 = lift_ugen_2 Sc3.mce2

mix :: Monad m => Ugen_M m -> Ugen_M m
mix x = Ugen_M (mk_ugen x >>= return . Sc3.mix)

sum_opt :: Monad m => [Ugen_M m] -> Ugen_M m
sum_opt = sum_opt_f sum3 sum4

done_action_to_ugen :: Monad m => Sc3.DoneAction (Ugen_M m) -> m Sc3.Ugen
done_action_to_ugen = mk_ugen . from_done_action

envelope_to_ugen :: Monad m => Sc3.Envelope (Ugen_M m) -> m Sc3.Ugen
envelope_to_ugen =
    let err = error "envGen: bad Envelope"
    in fmap Sc3.mce . sequence . map mk_ugen . fromMaybe err . Sc3.envelope_sc3_array

warp_to_ugen :: Monad m => Sc3.Warp (Ugen_M m) -> m Sc3.Ugen
warp_to_ugen = mk_ugen . from_warp

-- * AUTOGEN

a2k :: Monad m => Ugen_M m -> Ugen_M m
a2k = lift_ugen_1 Sc3.a2k

apf :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
apf = lift_ugen_3 Sc3.apf

allpassC :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
allpassC = lift_ugen_4 Sc3.allpassC

allpassL :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
allpassL = lift_ugen_4 Sc3.allpassL

allpassN :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
allpassN = lift_ugen_4 Sc3.allpassN

ampComp :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
ampComp rate = lift_ugen_3 (Sc3.ampComp rate)

ampCompA :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
ampCompA rate = lift_ugen_4 (Sc3.ampCompA rate)

amplitude :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
amplitude rate = lift_ugen_3 (Sc3.amplitude rate)

bAllPass :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
bAllPass = lift_ugen_3 Sc3.bAllPass

bBandPass :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
bBandPass = lift_ugen_3 Sc3.bBandPass

bBandStop :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
bBandStop = lift_ugen_3 Sc3.bBandStop

bHiPass :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
bHiPass = lift_ugen_3 Sc3.bHiPass

bHiShelf :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
bHiShelf = lift_ugen_4 Sc3.bHiShelf

bLowPass :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
bLowPass = lift_ugen_3 Sc3.bLowPass

bLowShelf :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
bLowShelf = lift_ugen_4 Sc3.bLowShelf

bpf :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
bpf = lift_ugen_3 Sc3.bpf

bpz2 :: Monad m => Ugen_M m -> Ugen_M m
bpz2 = lift_ugen_1 Sc3.bpz2

bPeakEQ :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
bPeakEQ = lift_ugen_4 Sc3.bPeakEQ

brf :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
brf = lift_ugen_3 Sc3.brf

brz2 :: Monad m => Ugen_M m -> Ugen_M m
brz2 = lift_ugen_1 Sc3.brz2

balance2 :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
balance2 = lift_ugen_4 Sc3.balance2

ball :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
ball rate = lift_ugen_4 (Sc3.ball rate)

beatTrack :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m
beatTrack rate = lift_ugen_2 (Sc3.beatTrack rate)

beatTrack2 :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
beatTrack2 rate = lift_ugen_6 (Sc3.beatTrack2 rate)

biPanB2 :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
biPanB2 rate = lift_ugen_4 (Sc3.biPanB2 rate)

binaryOpUgen :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
binaryOpUgen = lift_ugen_2 Sc3.binaryOpUgen

blip :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m
blip rate = lift_ugen_2 (Sc3.blip rate)

blockSize :: Monad m => Ugen_M m
blockSize = lift_ugen_0 Sc3.blockSize

brownNoise :: Uid m => Rate -> Ugen_M m
brownNoise rate = lift_ugenM_0 (Sc3.brownNoiseM rate)

bufAllpassC :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
bufAllpassC = lift_ugen_4 Sc3.bufAllpassC

bufAllpassL :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
bufAllpassL = lift_ugen_4 Sc3.bufAllpassL

bufAllpassN :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
bufAllpassN = lift_ugen_4 Sc3.bufAllpassN

bufChannels :: Monad m => Rate -> Ugen_M m -> Ugen_M m
bufChannels rate = lift_ugen_1 (Sc3.bufChannels rate)

bufCombC :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
bufCombC = lift_ugen_4 Sc3.bufCombC

bufCombL :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
bufCombL = lift_ugen_4 Sc3.bufCombL

bufCombN :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
bufCombN = lift_ugen_4 Sc3.bufCombN

bufDelayC :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
bufDelayC = lift_ugen_3 Sc3.bufDelayC

bufDelayL :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
bufDelayL = lift_ugen_3 Sc3.bufDelayL

bufDelayN :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
bufDelayN = lift_ugen_3 Sc3.bufDelayN

bufDur :: Monad m => Rate -> Ugen_M m -> Ugen_M m
bufDur rate = lift_ugen_1 (Sc3.bufDur rate)

bufFrames :: Monad m => Rate -> Ugen_M m -> Ugen_M m
bufFrames rate = lift_ugen_1 (Sc3.bufFrames rate)

bufRateScale :: Monad m => Rate -> Ugen_M m -> Ugen_M m
bufRateScale rate = lift_ugen_1 (Sc3.bufRateScale rate)

--bufRd :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
--bufRd rate = lift_ugen_4 (Sc3.bufRd rate)

bufSampleRate :: Monad m => Rate -> Ugen_M m -> Ugen_M m
bufSampleRate rate = lift_ugen_1 (Sc3.bufSampleRate rate)

bufSamples :: Monad m => Rate -> Ugen_M m -> Ugen_M m
bufSamples rate = lift_ugen_1 (Sc3.bufSamples rate)

--bufWr :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
--bufWr = lift_ugen_4 Sc3.bufWr

cOsc :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
cOsc rate = lift_ugen_3 (Sc3.cOsc rate)

checkBadValues :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
checkBadValues = lift_ugen_3 Sc3.checkBadValues

clip :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
clip = lift_ugen_3 Sc3.clip

clipNoise :: Uid m => Rate -> Ugen_M m
clipNoise rate = lift_ugenM_0 (Sc3.clipNoiseM rate)

coinGate :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m
coinGate = lift_ugenM_2 Sc3.coinGateM

combC :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
combC = lift_ugen_4 Sc3.combC

combL :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
combL = lift_ugen_4 Sc3.combL

combN :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
combN = lift_ugen_4 Sc3.combN

compander :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
compander = lift_ugen_7 Sc3.compander

companderD :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
companderD rate = lift_ugen_6 (Sc3.companderD rate)

controlDur :: Monad m => Ugen_M m
controlDur = lift_ugen_0 Sc3.controlDur

controlRate :: Monad m => Ugen_M m
controlRate = lift_ugen_0 Sc3.controlRate

convolution :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
convolution = lift_ugen_3 Sc3.convolution

convolution2 :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
convolution2 = lift_ugen_4 Sc3.convolution2

convolution2L :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
convolution2L rate = lift_ugen_5 (Sc3.convolution2L rate)

convolution3 :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
convolution3 rate = lift_ugen_4 (Sc3.convolution3 rate)

crackle :: Monad m => Rate -> Ugen_M m -> Ugen_M m
crackle rate = lift_ugen_1 (Sc3.crackle rate)

cuspL :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
cuspL rate = lift_ugen_4 (Sc3.cuspL rate)

cuspN :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
cuspN rate = lift_ugen_4 (Sc3.cuspN rate)

dc :: Monad m => Rate -> Ugen_M m -> Ugen_M m
dc rate = lift_ugen_1 (Sc3.dc rate)

dbrown :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
dbrown = lift_ugenM_4 Sc3.dbrownM

--dbufrd :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
--dbufrd = lift_ugenM_3 Sc3.dbufrdM

--dbufwr :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
--dbufwr = lift_ugenM_4 Sc3.dbufwrM

dconst :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
dconst = lift_ugenM_3 Sc3.dconstM

decay :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
decay = lift_ugen_2 Sc3.decay

decay2 :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
decay2 = lift_ugen_3 Sc3.decay2

decodeB2 :: Monad m => Int -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
decodeB2 numChannels = lift_ugen_4 (Sc3.decodeB2 numChannels)

degreeToKey :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
degreeToKey = lift_ugen_3 Sc3.degreeToKey

delTapRd :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
delTapRd = lift_ugen_4 Sc3.delTapRd

delTapWr :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
delTapWr = lift_ugen_2 Sc3.delTapWr

delay1 :: Monad m => Ugen_M m -> Ugen_M m
delay1 = lift_ugen_1 Sc3.delay1

delay2 :: Monad m => Ugen_M m -> Ugen_M m
delay2 = lift_ugen_1 Sc3.delay2

delayC :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
delayC = lift_ugen_3 Sc3.delayC

delayL :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
delayL = lift_ugen_3 Sc3.delayL

delayN :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
delayN = lift_ugen_3 Sc3.delayN

demand :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
demand = lift_ugen_3 Sc3.demand

--demandEnvGen :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
--demandEnvGen rate = lift_ugen_10 (Sc3.demandEnvGen rate)

detectIndex :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
detectIndex = lift_ugen_2 Sc3.detectIndex

--detectSilence :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
--detectSilence = lift_ugen_4 Sc3.detectSilence

dgeom :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
dgeom = lift_ugenM_3 Sc3.dgeomM

dibrown :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
dibrown = lift_ugenM_4 Sc3.dibrownM

--diskIn :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
--diskIn = lift_ugen_2 Sc3.diskIn

diskOut :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
diskOut = lift_ugen_2 Sc3.diskOut

diwhite :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
diwhite = lift_ugenM_3 Sc3.diwhiteM

done :: Monad m => Ugen_M m -> Ugen_M m
done = lift_ugen_1 Sc3.done

dpoll :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
dpoll = lift_ugenM_4 Sc3.dpollM

drand :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m
drand = lift_ugenM_2 Sc3.drandM

dreset :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m
dreset = lift_ugenM_2 Sc3.dresetM

dseq :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m
dseq = lift_ugenM_2 Sc3.dseqM

dser :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m
dser = lift_ugenM_2 Sc3.dserM

dseries :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
dseries = lift_ugenM_3 Sc3.dseriesM

dshuf :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m
dshuf = lift_ugenM_2 Sc3.dshufM

dstutter :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m
dstutter = lift_ugenM_2 Sc3.dstutterM

dswitch :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m
dswitch = lift_ugenM_2 Sc3.dswitchM

dswitch1 :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m
dswitch1 = lift_ugenM_2 Sc3.dswitch1M

dunique :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
dunique = lift_ugenM_3 Sc3.duniqueM

dust :: Uid m => Rate -> Ugen_M m -> Ugen_M m
dust rate = lift_ugenM_1 (Sc3.dustM rate)

dust2 :: Uid m => Rate -> Ugen_M m -> Ugen_M m
dust2 rate = lift_ugenM_1 (Sc3.dust2M rate)

--duty :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
--duty rate = lift_ugen_4 (Sc3.duty rate)

dwhite :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
dwhite = lift_ugenM_3 Sc3.dwhiteM

dwrand :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
dwrand = lift_ugenM_3 Sc3.dwrandM

dxrand :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m
dxrand = lift_ugenM_2 Sc3.dxrandM

envGen :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> DoneAction (Ugen_M m) -> Sc3.Envelope (Ugen_M m) -> Ugen_M m
envGen rate (Ugen_M p) (Ugen_M q) (Ugen_M r) (Ugen_M s) t u = Ugen_M $ do
  p' <- p
  q' <- q
  r' <- r
  s' <- s
  t' <- done_action_to_ugen t
  u' <- envelope_to_ugen u
  return (Sc3.envGen_ll rate p' q' r' s' t' u')

expRand :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m
expRand = lift_ugenM_2 Sc3.expRandM

fbSineC :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
fbSineC rate = lift_ugen_7 (Sc3.fbSineC rate)

fbSineL :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
fbSineL rate = lift_ugen_7 (Sc3.fbSineL rate)

fbSineN :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
fbSineN rate = lift_ugen_7 (Sc3.fbSineN rate)

fft :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
fft = lift_ugen_6 Sc3.fft

fos :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
fos = lift_ugen_4 Sc3.fos

fSinOsc :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m
fSinOsc rate = lift_ugen_2 (Sc3.fSinOsc rate)

fold :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
fold = lift_ugen_3 Sc3.fold

formant :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
formant rate = lift_ugen_3 (Sc3.formant rate)

formlet :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
formlet = lift_ugen_4 Sc3.formlet

free :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
free = lift_ugen_2 Sc3.free

freeSelf :: Monad m => Ugen_M m -> Ugen_M m
freeSelf = lift_ugen_1 Sc3.freeSelf

freeSelfWhenDone :: Monad m => Ugen_M m -> Ugen_M m
freeSelfWhenDone = lift_ugen_1 Sc3.freeSelfWhenDone

freeVerb :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
freeVerb = lift_ugen_4 Sc3.freeVerb

freeVerb2 :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
freeVerb2 = lift_ugen_5 Sc3.freeVerb2

freqShift :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
freqShift = lift_ugen_3 Sc3.freqShift

gVerb :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
gVerb = lift_ugen_10 Sc3.gVerb

gate :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
gate = lift_ugen_2 Sc3.gate

gbmanL :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
gbmanL rate = lift_ugen_3 (Sc3.gbmanL rate)

gbmanN :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
gbmanN rate = lift_ugen_3 (Sc3.gbmanN rate)

{-
gendy1 :: Uid m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
gendy1 rate = lift_ugenM_10 (Sc3.gendy1M rate)

gendy2 :: Uid m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
gendy2 rate = lift_ugenM_12 (Sc3.gendy2M rate)

gendy3 :: Uid m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
gendy3 rate = lift_ugenM_9 (Sc3.gendy3M rate)
-}

grainBuf :: Monad m => Int -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
grainBuf numChannels = lift_ugen_9 (Sc3.grainBuf numChannels)

grainFM :: Monad m => Int -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
grainFM numChannels = lift_ugen_8 (Sc3.grainFM numChannels)

grainIn :: Monad m => Int -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
grainIn numChannels = lift_ugen_6 (Sc3.grainIn numChannels)

grainSin :: Monad m => Int -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
grainSin numChannels = lift_ugen_6 (Sc3.grainSin numChannels)

grayNoise :: Uid m => Rate -> Ugen_M m
grayNoise rate = lift_ugenM_0 (Sc3.grayNoiseM rate)

hpf :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
hpf = lift_ugen_2 Sc3.hpf

hpz1 :: Monad m => Ugen_M m -> Ugen_M m
hpz1 = lift_ugen_1 Sc3.hpz1

hpz2 :: Monad m => Ugen_M m -> Ugen_M m
hpz2 = lift_ugen_1 Sc3.hpz2

hasher :: Monad m => Ugen_M m -> Ugen_M m
hasher = lift_ugen_1 Sc3.hasher

henonC :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
henonC rate = lift_ugen_5 (Sc3.henonC rate)

henonL :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
henonL rate = lift_ugen_5 (Sc3.henonL rate)

henonN :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
henonN rate = lift_ugen_5 (Sc3.henonN rate)

hilbert :: Monad m => Ugen_M m -> Ugen_M m
hilbert = lift_ugen_1 Sc3.hilbert

--iEnvGen :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m
--iEnvGen rate = lift_ugen_2 (Sc3.iEnvGen rate)

ifft :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
ifft = lift_ugen_3 Sc3.ifft

iRand :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m
iRand = lift_ugenM_2 Sc3.iRandM

impulse :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m
impulse rate = lift_ugen_2 (Sc3.impulse rate)

in' :: Monad m => Int -> Rate -> Ugen_M m -> Ugen_M m
in' numChannels rate = lift_ugen_1 (Sc3.in' numChannels rate)

inFeedback :: Monad m => Int -> Ugen_M m -> Ugen_M m
inFeedback numChannels = lift_ugen_1 (Sc3.inFeedback numChannels)

inRange :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
inRange = lift_ugen_3 Sc3.inRange

inRect :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
inRect rate = lift_ugen_3 (Sc3.inRect rate)

inTrig :: Monad m => Int -> Ugen_M m -> Ugen_M m
inTrig numChannels = lift_ugen_1 (Sc3.inTrig numChannels)

index :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
index = lift_ugen_2 Sc3.index

indexInBetween :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
indexInBetween = lift_ugen_2 Sc3.indexInBetween

indexL :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
indexL = lift_ugen_2 Sc3.indexL

infoUgenBase :: Monad m => Rate -> Ugen_M m
infoUgenBase rate = lift_ugen_0 (Sc3.infoUgenBase rate)

integrator :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
integrator = lift_ugen_2 Sc3.integrator

k2a :: Monad m => Ugen_M m -> Ugen_M m
k2a = lift_ugen_1 Sc3.k2a

keyState :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
keyState rate = lift_ugen_4 (Sc3.keyState rate)

keyTrack :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
keyTrack rate = lift_ugen_3 (Sc3.keyTrack rate)

klang :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
klang rate = lift_ugen_3 (Sc3.klang rate)

klank :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
klank = lift_ugen_5 Sc3.klank

lfClipNoise :: Uid m => Rate -> Ugen_M m -> Ugen_M m
lfClipNoise rate = lift_ugenM_1 (Sc3.lfClipNoiseM rate)

lfCub :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m
lfCub rate = lift_ugen_2 (Sc3.lfCub rate)

lfdClipNoise :: Uid m => Rate -> Ugen_M m -> Ugen_M m
lfdClipNoise rate = lift_ugenM_1 (Sc3.lfdClipNoiseM rate)

lfdNoise0 :: Uid m => Rate -> Ugen_M m -> Ugen_M m
lfdNoise0 rate = lift_ugenM_1 (Sc3.lfdNoise0M rate)

lfdNoise1 :: Uid m => Rate -> Ugen_M m -> Ugen_M m
lfdNoise1 rate = lift_ugenM_1 (Sc3.lfdNoise1M rate)

lfdNoise3 :: Uid m => Rate -> Ugen_M m -> Ugen_M m
lfdNoise3 rate = lift_ugenM_1 (Sc3.lfdNoise3M rate)

--lfGauss :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
--lfGauss rate = lift_ugen_5 (Sc3.lfGauss rate)

lfNoise0 :: Uid m => Rate -> Ugen_M m -> Ugen_M m
lfNoise0 rate = lift_ugenM_1 (Sc3.lfNoise0M rate)

lfNoise1 :: Uid m => Rate -> Ugen_M m -> Ugen_M m
lfNoise1 rate = lift_ugenM_1 (Sc3.lfNoise1M rate)

lfNoise2 :: Uid m => Rate -> Ugen_M m -> Ugen_M m
lfNoise2 rate = lift_ugenM_1 (Sc3.lfNoise2M rate)

lfPar :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m
lfPar rate = lift_ugen_2 (Sc3.lfPar rate)

lfPulse :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
lfPulse rate = lift_ugen_3 (Sc3.lfPulse rate)

lfSaw :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m
lfSaw rate = lift_ugen_2 (Sc3.lfSaw rate)

lfTri :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m
lfTri rate = lift_ugen_2 (Sc3.lfTri rate)

lpf :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
lpf = lift_ugen_2 Sc3.lpf

lpz1 :: Monad m => Ugen_M m -> Ugen_M m
lpz1 = lift_ugen_1 Sc3.lpz1

lpz2 :: Monad m => Ugen_M m -> Ugen_M m
lpz2 = lift_ugen_1 Sc3.lpz2

lag :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
lag = lift_ugen_2 Sc3.lag

lag2 :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
lag2 = lift_ugen_2 Sc3.lag2

lag2UD :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
lag2UD = lift_ugen_3 Sc3.lag2UD

lag3 :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
lag3 = lift_ugen_2 Sc3.lag3

lag3UD :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
lag3UD = lift_ugen_3 Sc3.lag3UD

lagIn :: Monad m => Int -> Ugen_M m -> Ugen_M m -> Ugen_M m
lagIn numChannels = lift_ugen_2 (Sc3.lagIn numChannels)

lagUD :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
lagUD = lift_ugen_3 Sc3.lagUD

lastValue :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
lastValue = lift_ugen_2 Sc3.lastValue

latch :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
latch = lift_ugen_2 Sc3.latch

latoocarfianC :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
latoocarfianC rate = lift_ugen_7 (Sc3.latoocarfianC rate)

latoocarfianL :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
latoocarfianL rate = lift_ugen_7 (Sc3.latoocarfianL rate)

latoocarfianN :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
latoocarfianN rate = lift_ugen_7 (Sc3.latoocarfianN rate)

leakDC :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
leakDC = lift_ugen_2 Sc3.leakDC

leastChange :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m
leastChange rate = lift_ugen_2 (Sc3.leastChange rate)

limiter :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
limiter = lift_ugen_3 Sc3.limiter

linCongC :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
linCongC rate = lift_ugen_5 (Sc3.linCongC rate)

linCongL :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
linCongL rate = lift_ugen_5 (Sc3.linCongL rate)

linCongN :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
linCongN rate = lift_ugen_5 (Sc3.linCongN rate)

linExp :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
linExp = lift_ugen_5 Sc3.linExp

linPan2 :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
linPan2 = lift_ugen_3 Sc3.linPan2

linRand :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
linRand = lift_ugenM_3 Sc3.linRandM

linXFade2 :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
linXFade2 = lift_ugen_3 Sc3.linXFade2

--line :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
--line rate = lift_ugen_4 (Sc3.line rate)

--linen :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
--linen = lift_ugen_5 Sc3.linen

localBuf :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m
localBuf = lift_ugenM_2 Sc3.localBufM

localIn :: Monad m => Int -> Rate -> Ugen_M m -> Ugen_M m
localIn numChannels rate = lift_ugen_1 (Sc3.localIn numChannels rate)

localOut :: Monad m => Ugen_M m -> Ugen_M m
localOut = lift_ugen_1 Sc3.localOut

logistic :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
logistic rate = lift_ugen_3 (Sc3.logistic rate)

lorenzL :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
lorenzL rate = lift_ugen_8 (Sc3.lorenzL rate)

loudness :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
loudness = lift_ugen_3 Sc3.loudness

mfcc :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m
mfcc rate = lift_ugen_2 (Sc3.mfcc rate)

mantissaMask :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
mantissaMask = lift_ugen_2 Sc3.mantissaMask

median :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
median = lift_ugen_2 Sc3.median

midEQ :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
midEQ = lift_ugen_4 Sc3.midEQ

modDif :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
modDif = lift_ugen_3 Sc3.modDif

moogFF :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
moogFF = lift_ugen_4 Sc3.moogFF

mostChange :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
mostChange = lift_ugen_2 Sc3.mostChange

mouseButton :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
mouseButton rate = lift_ugen_3 (Sc3.mouseButton rate)

mouseX :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Warp (Ugen_M m) -> Ugen_M m -> Ugen_M m
mouseX rate (Ugen_M p) (Ugen_M q) r (Ugen_M s) = Ugen_M $ do
  p' <- p
  q' <- q
  r' <- warp_to_ugen r
  s' <- s
  return (Sc3.mouseX rate p' q' (Sc3.WithWarp r') s')

mouseY :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Warp (Ugen_M m) -> Ugen_M m -> Ugen_M m
mouseY rate (Ugen_M p) (Ugen_M q) r (Ugen_M s) = Ugen_M $ do
  p' <- p
  q' <- q
  r' <- warp_to_ugen r
  s' <- s
  return (Sc3.mouseY rate p' q' (Sc3.WithWarp r') s')

nRand :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
nRand = lift_ugenM_3 Sc3.nRandM

nodeID :: Monad m => Rate -> Ugen_M m
nodeID rate = lift_ugen_0 (Sc3.nodeID rate)

normalizer :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
normalizer = lift_ugen_3 Sc3.normalizer

numAudioBuses :: Monad m => Ugen_M m
numAudioBuses = lift_ugen_0 Sc3.numAudioBuses

numBuffers :: Monad m => Ugen_M m
numBuffers = lift_ugen_0 Sc3.numBuffers

numControlBuses :: Monad m => Ugen_M m
numControlBuses = lift_ugen_0 Sc3.numControlBuses

numInputBuses :: Monad m => Ugen_M m
numInputBuses = lift_ugen_0 Sc3.numInputBuses

numOutputBuses :: Monad m => Ugen_M m
numOutputBuses = lift_ugen_0 Sc3.numOutputBuses

numRunningSynths :: Monad m => Ugen_M m
numRunningSynths = lift_ugen_0 Sc3.numRunningSynths

offsetOut :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
offsetOut = lift_ugen_2 Sc3.offsetOut

onePole :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
onePole = lift_ugen_2 Sc3.onePole

oneZero :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
oneZero = lift_ugen_2 Sc3.oneZero

onsets :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
onsets = lift_ugen_9 Sc3.onsets

osc :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
osc rate = lift_ugen_3 (Sc3.osc rate)

oscN :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
oscN rate = lift_ugen_3 (Sc3.oscN rate)

out :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
out = lift_ugen_2 Sc3.out

pSinGrain :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
pSinGrain rate = lift_ugen_3 (Sc3.pSinGrain rate)

pv_Add :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
pv_Add = lift_ugen_2 Sc3.pv_Add

pv_BinScramble :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
pv_BinScramble = lift_ugenM_4 Sc3.pv_BinScrambleM

pv_BinShift :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
pv_BinShift = lift_ugen_4 Sc3.pv_BinShift

pv_BinWipe :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
pv_BinWipe = lift_ugen_3 Sc3.pv_BinWipe

pv_BrickWall :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
pv_BrickWall = lift_ugen_2 Sc3.pv_BrickWall

pv_ConformalMap :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
pv_ConformalMap = lift_ugen_3 Sc3.pv_ConformalMap

pv_Conj :: Monad m => Ugen_M m -> Ugen_M m
pv_Conj = lift_ugen_1 Sc3.pv_Conj

pv_Copy :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
pv_Copy = lift_ugen_2 Sc3.pv_Copy

pv_CopyPhase :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
pv_CopyPhase = lift_ugen_2 Sc3.pv_CopyPhase

pv_Diffuser :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
pv_Diffuser = lift_ugen_2 Sc3.pv_Diffuser

pv_Div :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
pv_Div = lift_ugen_2 Sc3.pv_Div

--pv_HainsworthFoote :: Monad m => Ugen_M m -> Ugen_M m
--pv_HainsworthFoote = lift_ugen_1 Sc3.pv_HainsworthFoote

--pv_JensenAndersen :: Monad m => Ugen_M m -> Ugen_M m
--pv_JensenAndersen = lift_ugen_1 Sc3.pv_JensenAndersen

pv_LocalMax :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
pv_LocalMax = lift_ugen_2 Sc3.pv_LocalMax

pv_MagAbove :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
pv_MagAbove = lift_ugen_2 Sc3.pv_MagAbove

pv_MagBelow :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
pv_MagBelow = lift_ugen_2 Sc3.pv_MagBelow

pv_MagClip :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
pv_MagClip = lift_ugen_2 Sc3.pv_MagClip

pv_MagDiv :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
pv_MagDiv = lift_ugen_3 Sc3.pv_MagDiv

pv_MagFreeze :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
pv_MagFreeze = lift_ugen_2 Sc3.pv_MagFreeze

pv_MagMul :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
pv_MagMul = lift_ugen_2 Sc3.pv_MagMul

pv_MagNoise :: Monad m => Ugen_M m -> Ugen_M m
pv_MagNoise = lift_ugen_1 Sc3.pv_MagNoise

pv_MagShift :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
pv_MagShift = lift_ugen_3 Sc3.pv_MagShift

pv_MagSmear :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
pv_MagSmear = lift_ugen_2 Sc3.pv_MagSmear

pv_MagSquared :: Monad m => Ugen_M m -> Ugen_M m
pv_MagSquared = lift_ugen_1 Sc3.pv_MagSquared

pv_Max :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
pv_Max = lift_ugen_2 Sc3.pv_Max

pv_Min :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
pv_Min = lift_ugen_2 Sc3.pv_Min

pv_Mul :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
pv_Mul = lift_ugen_2 Sc3.pv_Mul

pv_PhaseShift :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
pv_PhaseShift = lift_ugen_3 Sc3.pv_PhaseShift

pv_PhaseShift270 :: Monad m => Ugen_M m -> Ugen_M m
pv_PhaseShift270 = lift_ugen_1 Sc3.pv_PhaseShift270

pv_PhaseShift90 :: Monad m => Ugen_M m -> Ugen_M m
pv_PhaseShift90 = lift_ugen_1 Sc3.pv_PhaseShift90

pv_RandComb :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
pv_RandComb = lift_ugenM_3 Sc3.pv_RandCombM

pv_RandWipe :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
pv_RandWipe = lift_ugenM_4 Sc3.pv_RandWipeM

pv_RectComb :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
pv_RectComb = lift_ugen_4 Sc3.pv_RectComb

pv_RectComb2 :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
pv_RectComb2 = lift_ugen_5 Sc3.pv_RectComb2

pan2 :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
pan2 = lift_ugen_3 Sc3.pan2

pan4 :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
pan4 rate = lift_ugen_4 (Sc3.pan4 rate)

panAz :: Monad m => Int -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
panAz numChannels  = lift_ugen_5 (Sc3.panAz numChannels)

panB :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
panB rate = lift_ugen_4 (Sc3.panB rate)

panB2 :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
panB2 = lift_ugen_3 Sc3.panB2

partConv :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
partConv = lift_ugen_3 Sc3.partConv

pause :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
pause = lift_ugen_2 Sc3.pause

pauseSelf :: Monad m => Ugen_M m -> Ugen_M m
pauseSelf = lift_ugen_1 Sc3.pauseSelf

pauseSelfWhenDone :: Monad m => Ugen_M m -> Ugen_M m
pauseSelfWhenDone = lift_ugen_1 Sc3.pauseSelfWhenDone

peak :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
peak = lift_ugen_2 Sc3.peak

peakFollower :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
peakFollower = lift_ugen_2 Sc3.peakFollower

phasor :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
phasor rate = lift_ugen_5 (Sc3.phasor rate)

pinkNoise :: Uid m => Rate -> Ugen_M m
pinkNoise rate = lift_ugenM_0 (Sc3.pinkNoiseM rate)

--pitch :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
--pitch = lift_ugen_11 Sc3.pitch

pitchShift :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
pitchShift = lift_ugen_5 Sc3.pitchShift

--playBuf :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
--playBuf rate = lift_ugen_6 (Sc3.playBuf rate)

pluck :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
pluck = lift_ugen_6 Sc3.pluck

poll :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
poll = lift_ugen_4 Sc3.poll

pulse :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m
pulse rate = lift_ugen_2 (Sc3.pulse rate)

pulseCount :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
pulseCount = lift_ugen_2 Sc3.pulseCount

pulseDivider :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
pulseDivider = lift_ugen_3 Sc3.pulseDivider

quadC :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
quadC rate = lift_ugen_5 (Sc3.quadC rate)

quadL :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
quadL rate = lift_ugen_5 (Sc3.quadL rate)

quadN :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
quadN rate = lift_ugen_5 (Sc3.quadN rate)

rhpf :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
rhpf = lift_ugen_3 Sc3.rhpf

rlpf :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
rlpf = lift_ugen_3 Sc3.rlpf

radiansPerSample :: Monad m => Ugen_M m
radiansPerSample = lift_ugen_0 Sc3.radiansPerSample

ramp :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
ramp = lift_ugen_2 Sc3.ramp

rand :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m
rand = lift_ugenM_2 Sc3.randM

randID :: Monad m => Rate -> Ugen_M m -> Ugen_M m
randID rate = lift_ugen_1 (Sc3.randID rate)

randSeed :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m
randSeed rate = lift_ugen_2 (Sc3.randSeed rate)

--recordBuf :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
--recordBuf rate = lift_ugen_9 (Sc3.recordBuf rate)

replaceOut :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
replaceOut = lift_ugen_2 Sc3.replaceOut

resonz :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
resonz = lift_ugen_3 Sc3.resonz

ringz :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
ringz = lift_ugen_3 Sc3.ringz

rotate2 :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
rotate2 = lift_ugen_3 Sc3.rotate2

runningMax :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
runningMax = lift_ugen_2 Sc3.runningMax

runningMin :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
runningMin = lift_ugen_2 Sc3.runningMin

runningSum :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
runningSum = lift_ugen_2 Sc3.runningSum

sos :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
sos = lift_ugen_6 Sc3.sos

sampleDur :: Monad m => Ugen_M m
sampleDur = lift_ugen_0 Sc3.sampleDur

sampleRate :: Monad m => Ugen_M m
sampleRate = lift_ugen_0 Sc3.sampleRate

saw :: Monad m => Rate -> Ugen_M m -> Ugen_M m
saw rate = lift_ugen_1 (Sc3.saw rate)

schmidt :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
schmidt = lift_ugen_3 Sc3.schmidt

select :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
select = lift_ugen_2 Sc3.select

sendTrig :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
sendTrig = lift_ugen_3 Sc3.sendTrig

setResetFF :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
setResetFF = lift_ugen_2 Sc3.setResetFF

shaper :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
shaper = lift_ugen_2 Sc3.shaper

sinOsc :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m
sinOsc rate = lift_ugen_2 (Sc3.sinOsc rate)

sinOscFB :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m
sinOscFB rate = lift_ugen_2 (Sc3.sinOscFB rate)

slew :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
slew = lift_ugen_3 Sc3.slew

slope :: Monad m => Ugen_M m -> Ugen_M m
slope = lift_ugen_1 Sc3.slope

specCentroid :: Monad m => Rate -> Ugen_M m -> Ugen_M m
specCentroid rate = lift_ugen_1 (Sc3.specCentroid rate)

specFlatness :: Monad m => Rate -> Ugen_M m -> Ugen_M m
specFlatness rate = lift_ugen_1 (Sc3.specFlatness rate)

specPcile :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
specPcile rate = lift_ugen_3 (Sc3.specPcile rate)

spring :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
spring rate = lift_ugen_3 (Sc3.spring rate)

standardL :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
standardL rate = lift_ugen_4 (Sc3.standardL rate)

standardN :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
standardN rate = lift_ugen_4 (Sc3.standardN rate)

stepper :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
stepper = lift_ugen_6 Sc3.stepper

stereoConvolution2L :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
stereoConvolution2L rate = lift_ugen_6 (Sc3.stereoConvolution2L rate)

subsampleOffset :: Monad m => Ugen_M m
subsampleOffset = lift_ugen_0 Sc3.subsampleOffset

sum3 :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
sum3 = lift_ugen_3 Sc3.sum3

sum4 :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
sum4 = lift_ugen_4 Sc3.sum4

sweep :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m
sweep rate = lift_ugen_2 (Sc3.sweep rate)

syncSaw :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m
syncSaw rate = lift_ugen_2 (Sc3.syncSaw rate)

t2a :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
t2a = lift_ugen_2 Sc3.t2a

t2k :: Monad m => Ugen_M m -> Ugen_M m
t2k = lift_ugen_1 Sc3.t2k

tBall :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
tBall rate = lift_ugen_4 (Sc3.tBall rate)

tDelay :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
tDelay = lift_ugen_2 Sc3.tDelay

--tDuty :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
--tDuty rate = lift_ugen_5 (Sc3.tDuty rate)

tExpRand :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
tExpRand = lift_ugenM_3 Sc3.tExpRandM

tGrains :: Monad m => Int -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
tGrains numChannels = lift_ugen_8 (Sc3.tGrains numChannels)

tiRand :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
tiRand = lift_ugenM_3 Sc3.tiRandM

tRand :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
tRand = lift_ugenM_3 Sc3.tRandM

tWindex :: Uid m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
tWindex = lift_ugenM_3 Sc3.tWindexM

timer :: Monad m => Ugen_M m -> Ugen_M m
timer = lift_ugen_1 Sc3.timer

toggleFF :: Monad m => Ugen_M m -> Ugen_M m
toggleFF = lift_ugen_1 Sc3.toggleFF

trig :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
trig = lift_ugen_2 Sc3.trig

trig1 :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
trig1 = lift_ugen_2 Sc3.trig1

twoPole :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
twoPole = lift_ugen_3 Sc3.twoPole

twoZero :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
twoZero = lift_ugen_3 Sc3.twoZero

--vDiskIn :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
--vDiskIn = lift_ugen_4 Sc3.vDiskIn

vOsc :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
vOsc rate = lift_ugen_3 (Sc3.vOsc rate)

vOsc3 :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
vOsc3 rate = lift_ugen_4 (Sc3.vOsc3 rate)

varLag :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
varLag = lift_ugen_5 Sc3.varLag

varSaw :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
varSaw rate = lift_ugen_3 (Sc3.varSaw rate)

--vibrato :: Uid m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
--vibrato rate = lift_ugenM_9 (Sc3.vibratoM rate)

warp1 :: Monad m => Int -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
warp1 numChannels = lift_ugen_8 (Sc3.warp1 numChannels)

whiteNoise :: Uid m => Rate -> Ugen_M m
whiteNoise rate = lift_ugenM_0 (Sc3.whiteNoiseM rate)

wrap :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
wrap = lift_ugen_3 Sc3.wrap

wrapIndex :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m
wrapIndex = lift_ugen_2 Sc3.wrapIndex

xFade2 :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
xFade2 = lift_ugen_4 Sc3.xFade2

--xLine :: Monad m => Rate -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
--xLine rate = lift_ugen_4 (Sc3.xLine rate)

xOut :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
xOut = lift_ugen_3 Sc3.xOut

zeroCrossing :: Monad m => Ugen_M m -> Ugen_M m
zeroCrossing = lift_ugen_1 Sc3.zeroCrossing

maxLocalBufs :: Monad m => Ugen_M m -> Ugen_M m
maxLocalBufs = lift_ugen_1 Sc3.maxLocalBufs

mulAdd :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
mulAdd = lift_ugen_3 Sc3.mulAdd

setBuf :: Monad m => Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m -> Ugen_M m
setBuf = lift_ugen_4 Sc3.setBuf

