import Data.List {- base -}

import Sound.SC3.UGen.DB.Bindings.Haskell {- hsc3-db -}
import Sound.SC3.UGen.DB.Record {- hsc3-db -}

gen_arg :: U -> String
gen_arg u = intercalate " -> " (replicate (u_num_inputs u + 1) "UGen_M m")

gen_sig :: String -> U -> String
gen_sig nm u =
  let rt = if u_requires_rate u then "Rate -> " else ""
      md = if ugen_nondet u then "UId" else "Monad"
  in concat [nm," :: ",md," m => ",rt,gen_arg u]

gen_def :: String -> U -> String
gen_def nm u =
  let (nm',lift) = if ugen_nondet u
                   then (nm ++ "M","lift_ugenM_")
                   else (nm,"lift_ugen_")
  in if u_requires_rate u
     then concat [nm," rate = ",lift,show (u_num_inputs u)," (SC3.",nm', " rate)"]
     else concat [nm," = ",lift,show (u_num_inputs u)," SC3.",nm']

-- > mapM_ (putStrLn . unlines . gen_u) Sound.SC3.UGen.DB.ugen_db_core
gen_u :: U -> [String]
gen_u u =
  let nm = ugen_hs_name u
  in [gen_sig nm u,gen_def nm u]
