all:
	echo "hsc3-m"

mk-cmd:
	echo "hsc3-m - NIL"

clean:
	rm -Rf dist dist-newstyle *~

push-all:
	r.gitlab-push.sh hsc3-m
