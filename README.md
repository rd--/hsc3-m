hsc3-m
------

[haskell](http://haskell.org/)
[supercollider](http://audiosynth.com/)
([hsc3](http://rohandrape.net/?t=hsc3))
variant UGen constructors.

[hsc3-m](http://rohandrape.net/?t=hsc3-m) provides `Sound.SC3.UGen.M`.

- NOTES: [m.lhs](?t=hsc3-m&e=Help/m.lhs)

© [rohan drape](http://rohandrape.net/),
  2018-2021,
  [gpl](http://gnu.org/copyleft/)
