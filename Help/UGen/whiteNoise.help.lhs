> import Sound.SC3.Common {- hsc3 -}
> import Sound.SC3.UGen.M {- hsc3-m -}

> g_00 :: UGen
> g_00 =
>   let o = sinOsc AR 440 0
>       n = whiteNoise AR
>   in mce2 (o - o) (n - n) * 0.05

> g_01 :: UGen
> g_01 =
>   let n = share (whiteNoise AR)
>   in (n - n) * 0.05
