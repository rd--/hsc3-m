> import Sound.SC3.Common {- hsc3 -}
> import Sound.SC3.UGen.M {- hsc3-m -}

> e_01 :: Fractional n => Envelope n
> e_01 = env_circle_0 (Envelope [6000,700,100] [1,1] [EnvExp,EnvLin] Nothing Nothing 0)

> g_01 :: UGen
> g_01 =
>     let f = envGen KR 1 1 0 1 DoNothing e_01
>     in sinOsc AR f 0 * 0.1 + impulse AR 1 0 + whiteNoise AR * 0.01

